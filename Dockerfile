FROM centos:7
FROM python:2.7
FROM openjdk:11-slim

LABEL org.opencontainers.image.authors="maikel@itmagix.nl"

# Version
ENV SOAPUI_VERSION 5.7.0

# SoapUi
COPY src/entry_point.sh opt/bin/entry_point.sh
COPY src/server.py /opt/bin/server.py
COPY src/server_index.html /opt/bin/server_index.html

RUN chmod +x /opt/bin/entry_point.sh
RUN chmod +x /opt/bin/server.py

RUN apt update && apt install -y curl
 
RUN mkdir -p /opt && \
    curl https://s3.amazonaws.com/downloads.eviware/soapuios/${SOAPUI_VERSION}/SoapUI-${SOAPUI_VERSION}-linux-bin.tar.gz \
    | gunzip -c - | tar -xf - -C /opt && \
    ln -s /opt/SoapUI-${SOAPUI_VERSION} /opt/SoapUI

# DB Drivers
COPY src/ojdbc11.jar /opt/SoapUI/bin/ext/ojdbc11.jar
COPY src/h2-2.1.210.jar /opt/SoapUI/bin/ext/h2-2.1.210.jar

# Working dir
WORKDIR /opt/bin

# Environment
ENV PATH=${PATH}:/opt/soapUI/bin
EXPOSE 3000
CMD ["/opt/bin/entry_point.sh"]
